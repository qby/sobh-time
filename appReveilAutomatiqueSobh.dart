//Les deux alarmes pour Sobh $dateEtHeureDuProchainSobh sont bien/pas actives (vert si active, rouge sinon), vous devez/pas aller au lit (vert si pas, rouge sinon)
//  Recheck button (actualise le texte en haut)
//  Reset button (on supprime toutes alarmes de l'application et on crées les deux pour le prochain Sobh) (puis on recheck)
// Test dans 10 secondes lit et 20 secondes Sobh (on crées les alarmes et les supprimes juste après 30 secondes)

//Temps au lit :
//  00 h 00 m (défaut 8h)

//Localisation :
//  Geolocalisation
//  Barre de recherche (défaut géolocalisation ou aucune)

//Méthode de calcul :
//  Liste de selection (avec automatique [par rapport à localisation ou aucune])

//Sonnerie :
//  Au lit :
//    Liste de selection (avec automatique et browse [fichiers ou sonnerie téléphone])
// Sobh :
//    Liste de selection (avec automatique et browse [fichiers ou sonnerie téléphone])

//Langue :
//  Liste de selection (avec automatique)
//  Vous pouvez ajouter des langues en contribuant au projet

//Il faut que l'application résiste aux mise en veille/extinctions/rafraichissement RAM/fermetures/économie de batterie/mode silencieux ne pas déranger

//Synchroniser avec le digital wellbeing/Bedtime mode/do not disturb
